
## Input/Output

Penilaian dilakukan secara otomatis oleh program, sehingga berbeda sedikit saja hasilnya juga akan salah. Misal anda harus mengeluarkan "Ristek" pada satu baris, maka:
```
Ristek
```

benar, namun
```
RisteK
```
salah (perhatikan bahwa perbedaannya hanya di karakter terakhir). Bahkan
```
Ristek_
```
dengan _ adalah spasi, juga salah.

Selain itu anda tidak perlu menambahkan, misal "Masukkan N: ". Hal ini karena "Masukkan N: " akan masuk ke berkas output. Contoh permasalahan, diberikan N, keluarkan N+1. Maka misal:

input
```
5
```
Di berkas output yang akan keluar adalah:
```
Masukkan N: 
6
```
Padahal seharusnya:
```
6
```
Selain itu:

-   Pastikan di akhir baris diakhiri tepat satu karakter newline ('\n').
-   Pastikan tidak ada spasi berlebih, di manapun itu.

Semoga membantu.

## I/O Redirection

Dikarenakan seluruh tes uji diberikan pada setiap soal, Anda dapat mencoba seluruh tes uji tersebut pada komputer Anda terlebih dahulu sebelum melakukan pengumpulan.

1.  Unduh kasus uji (zip), kemudian ekstrak file tersebut
2.  Pindahkan seluruh file input yang telah di-extract ke dalam direktori yang sama dengan program solusi yang anda buat
3.  Buka command prompt/terminal, arahkan ke dalam direktori program solusi
4.  lalu jalankan perintah berikut:

C++ (setelah di-compile):
```
<program_solusi>.exe < <file_input>.in
```
Java (setelah di-compile):
```
java <program_solusi> < <file_input>.in
```
Python:
```
python <program_solusi>.py < <file_input>.in    
```
Contoh:  
Program Solusi :  `solve.cpp` (C++) /  `Solve.java`  (Java) /  `solve.py` (Python)  
File input :  `pangkat-easy_1.in`

C++ (di-compile menjadi solve.exe)
```
solve.exe < pangkat-easy_1.in
```
Java:
```
java Solve < pangkat-easy_1.in
```
Python:
```
python solve.py < pangkat-easy_1.in
```
