Donat Enak

[Deskripsi Soal]

Salman si petualang baru-baru ini gemar mengoleksi N buah donat.
Setiap donat yang dikoleksi Salman memiliki sejumlah C_i chocochips di mana i menunjukkan donat ke-i.
Salman ingin memberikan 5 buah donatnya yang paling enak kepada Salma.
Diketahui bahwa donat yang paling enak merupakan donat dengan jumlah chocochips paling sedikit.
Sayangnya, Salma hanya ingin menerima pemberian donat-donat dengan total chocochips ganjil.

Sebagai contoh, jika ada donat-donat dengan chocochips 13, 9, 11, 3, 5, 7, dan 1 maka Salman akan memberi donat 9, 3, 5, 7, dan 1. Sedangkan, jika ada donat-donat dengan chocochips 2, 2, 2, 2 dan 4 maka Salman tidak dapat memenuhi kriteria Salma.

Tentukanlah berapa total chocochips yang akan diterima Salma dari Salman jika Salman dapat memenuhi kriteria Salma.

[Format Masukan]

Baris pertama berisi label kasus uji.
Label kasus uji adalah suatu string "#S" dimana S adalah nomor kasus uji tersebut. Kasus uji contoh adalah kasus uji nomor 0.

Baris kedua berisi N, banyak koleksi donat yang dimiliki Salman.
Baris ketiga berisi N buah bilangan terurut naik yang merepresentasikan C_i, jumlah chocochips donat ke-i yang dimiliki Salman.


[Format Keluaran]

Keluarkan total chocochips yang akan diterima Salma dari Salman atau -1 jika Salman tidak dapat memenuhi kriteria Salma.

[Contoh Masukan 1]

#0
7
13 11 9 7 5 3 1


[Contoh Keluaran 1

25

[Batasan Easy]

- 5 ≤ N ≤ 19
- 1 ≤ C_i ≤ 10^8
- N adalah bilangan prima
- Semua C_i ganjil


[Batasan Hard]

- 5 ≤ N ≤ 10^5
- 1 ≤ C_i ≤ 10^17
