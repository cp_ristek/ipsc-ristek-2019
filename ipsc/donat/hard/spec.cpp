#include <tcframe/spec.hpp>
#define pb push_back

using namespace tcframe;
typedef long long LL;

class ProblemSpec : public BaseProblemSpec {
protected:
	string LABEL;
	LL N, ANS;
	vector<LL> A;

	void InputFormat() {
		LINE(LABEL);
		LINE(N);
		LINE(A % SIZE(N));
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(5 <= N && N <= 100000);
		CONS(eachElementBetween(A, 1, (LL)1e17));
	}

private:
	bool eachElementBetween(vector<LL> &v, LL l, LL r) {
		for (LL num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:
	const LL MAXX = (LL)1e17;
	const LL MAXN = (LL)1e5;
	void SampleTestCase1() {
		Input({
			"#0",
			"7",
			"13 11 9 7 5 3 1"
			});
		Output({"25"});
	}

	void TestGroup1() {
		CASE(create_label(1), N = rnd.nextLongLong(5, MAXN) , A = randomVector(N, 1, MAXX));
		CASE(create_label(2), N = rnd.nextLongLong(5, MAXN), A = randomVector(N, 1, MAXX, 0));
		CASE(create_label(3), N = rnd.nextLongLong(5, MAXN), A = randomVector(N, 1, MAXX, 1));
		for (int i = 0; i < 4; i++) {
			CASE(create_label(4+i), N = rnd.nextLongLong(MAXN/10, MAXN) , A = randomVector(N, 1, MAXX));
		}
		CASE(create_label(8), N = rnd.nextLongLong(5, MAXN), A = randomVector(N, 1, MAXX, 2));
		CASE(create_label(9), N = rnd.nextLongLong(5, MAXN), A = randomVector(N, 1, MAXX, 4));
		CASE(create_label(10), N = 100000, A = randomVector(N, 1, MAXX));
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}

	vector<LL> randomVector(LL N, LL lo, LL hi, LL numJil = -1) {
		if (numJil == -1) numJil = rnd.nextLongLong(0, N);

		vector<LL> ans;
		LL ran = (hi - lo) / 2;
		for (LL i = 0; i < numJil; i++) {
			ans.pb(lo + 2 * rnd.nextLongLong(0, ran));
		}
		for (LL i = numJil; i < N; i++) {
			ans.pb(lo + 1 + 2 * rnd.nextLongLong(0, ran - 1));
		}

		rnd.shuffle(ans.begin(), ans.begin() + N);
		return ans;
	}
};
