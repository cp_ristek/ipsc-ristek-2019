#include <bits/stdc++.h>

using namespace std;
typedef long long LL;

LL N, A[100000];
int main() {
	ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);

	string label;
	cin >> label;
	cin >> N;
	for (LL i = 0; i < N; i++) cin >> A[i];

	sort(A, A + N);

	LL numJil = 0, numNap = 0;
	LL ans = -1;
	// 5 ganjil
	LL tmpAns = 0;
	numJil = numNap = 0;
	for (LL i = 0; i < N; i++) {
		if (A[i] % 2 == 1 && numJil < 5) {
			tmpAns += A[i];
			numJil++;
		}
	}
	if (numJil == 5) {
		ans = (ans == -1? tmpAns : min(ans, tmpAns));
	}

	// 3 ganjil
	tmpAns = 0;
	numJil = numNap = 0;
	for (LL i = 0; i < N; i++) {
		if (A[i] % 2 == 1 && numJil < 3) {
			tmpAns += A[i];
			numJil++;
		} else if (A[i] % 2 == 0 && numNap < 2) {
			tmpAns += A[i];
			numNap++;
		}
	}
	if (numJil == 3 && numNap == 2) {
		ans = (ans == -1? tmpAns : min(ans, tmpAns));
	}

	// 1 ganjil
	tmpAns = 0;
	numJil = numNap = 0;
	for (LL i = 0; i < N; i++) {
		if (A[i] % 2 == 1 && numJil < 1) {
			tmpAns += A[i];
			numJil++;
		} else if (A[i] % 2 == 0 && numNap < 4) {
			tmpAns += A[i];
			numNap++;
		}
	}
	if (numJil == 1 && numNap == 4) {
		ans = (ans == -1? tmpAns : min(ans, tmpAns));
	}

	cout << ans << endl;
	return 0;
}
