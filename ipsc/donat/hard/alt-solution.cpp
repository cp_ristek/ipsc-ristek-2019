#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXN = 100005;

int N;
ll C[MAXN];

int main() {
    ios::sync_with_stdio(0); cin.tie(0);

    string label;
    cin >> label;
    cin >> N;
    for (int i = 0; i < N; i++) {
        cin >> C[i];
    }
    sort(C, C+N);

    ll ganjil = -1;
    ll genap = -1;
    ll sum = 0;
    for (int i = 0; i < 5; i++) {
        if (C[i]&1) ganjil = C[i];
        else genap = C[i];
        sum += C[i];
    }
    if (sum&1) {
        cout << sum << "\n";
        return 0;
    }

    ll ans = 1e18;
    for (int i = 5; i < N; i++) {
        if (C[i]&1) {
            if (genap != -1) {
                ans = min(ans, sum - genap + C[i]);
            }
        } else {
            if (ganjil != -1) {
                ans = min(ans, sum - ganjil + C[i]);
            }
        }
    }
    if (ans == 1e18) ans = -1;
    cout << ans << "\n";

    return 0;
}
