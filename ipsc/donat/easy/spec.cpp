#include <tcframe/spec.hpp>
#define pb push_back

using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	string LABEL;
	int N, ANS;
	vector<int> A;

	void InputFormat() {
		LINE(LABEL);
		LINE(N);
		LINE(A % SIZE(N));
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(5 <= N && N <= 19);
		CONS(eachElementBetween(A, 1, (int)1e8));
		CONS(isPrime(N));
	}

private:
	bool eachElementBetween(vector<int> &v, int l, int r) {
		for (int num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}
	bool isPrime(int N) {
		for (int i = 2; i * i <= N; i++) {
			if (N % i == 0) {
				return false;
			}
		}

		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:
	const int MAXX = (int)1e8;
	void SampleTestCase1() {
		Input({
			"#0",
			"7",
			"13 11 9 7 5 3 1"
			});
		Output({"25"});
	}

	void TestGroup1() {
		CASE(create_label(1), N = 7, A = randomOddVector(N, 1, MAXX));
		CASE(create_label(2), N = 11, A = randomOddVector(N, 1, MAXX));
		CASE(create_label(3), N = 13, A = randomOddVector(N, 1, MAXX));
		CASE(create_label(4), N = 17, A = randomOddVector(N, 1, MAXX));
		CASE(create_label(5), N = 19, A = randomOddVector(N, 1, MAXX));
		CASE(create_label(6), N = 5, A = randomOddVector(N, 1, MAXX));
		CASE(create_label(7), N = 11, A = randomOddVector(N, 1, MAXX));
		CASE(create_label(8), N = 13, A = randomOddVector(N, 1, MAXX));
		CASE(create_label(9), N = 17, A = randomOddVector(N, 1, MAXX));
		CASE(create_label(10), N = 19, A = randomOddVector(N, 1, MAXX));
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}

	vector<int> randomOddVector(int N, int lo, int hi) {
		vector<int> ans;
		int ran = (hi - lo) / 2;
		for (int i = 0; i < N; i++) {
			ans.pb(lo + 2 * rnd.nextInt(0, ran));
		}
		return ans;
	}
};
