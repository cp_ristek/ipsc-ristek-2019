#include <bits/stdc++.h>

using namespace std;

int N, A[19];
int main() {
	ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);

	string label;
	cin >> label;
	cin >> N;
	for (int i = 0; i < N; i++) cin >> A[i];

	for (int i = 0; i < 5; i++) {
		int mini = A[i], id = i;
		for (int j = i + 1; j < N; j++) {
			if (A[j] < mini) {
				mini = A[j];
				id = j;
			}
		}
		swap(A[i], A[id]);
	}

	int sum = 0;
	for (int i = 0; i < 5; i++) sum += A[i];

	cout << sum << endl;
	return 0;
}
