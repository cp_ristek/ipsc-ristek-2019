#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

bool check(string &s, int idx) {
    string t = "salman";
    for (int i = 0; i < (int)t.size(); i++) {
        if (s[idx+i] != t[i]) return false;
    }
    return true;
}

int main() {
    string label; cin >> label;
    string s; cin >> s;
    int n = s.size();

    long long ans = 0;
    for (int i = 0; i < n-5; i++) {
        if (check(s, i)) {
            ans = (ll)(i+1)*(n-i-5);
            break;
        }
    }

    cout << ans << endl;
}
