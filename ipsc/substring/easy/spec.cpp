#include <tcframe/spec.hpp>
using namespace tcframe;

#include <bits/stdc++.h>
using namespace std;

const int N_MAX = 1e6;

class ProblemSpec : public BaseProblemSpec {
protected:
    string LABEL;
    int N; // For readability at TestCases()
    string S;

    long long result;

    void InputFormat() {
        LINE(LABEL);
        LINE(S);
    }

    void OutputFormat() {
        LINE(result);
    }

    void Constraints() {
        CONS(inBetween(S.length(), 1, 30));
        CONS(exactlyOneSalman(S));
    }

private:
    bool inBetween(int x, int lo, int hi) {
        return lo <= x && x <= hi;
    }

    bool exactlyOneSalman(string S) {
        int cntSalman = 0;
        for (int i = 0; i + 5 < (int)S.length(); i++) {
            if (S[i] == 's' && S[i+1] == 'a' && S[i+2] == 'l' &&
                    S[i+3] == 'm' && S[i+4] == 'a' && S[i+5] == 'n') {
                cntSalman++;
            }
        }
        return cntSalman == 1;
    }
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:
    void SampleTestCase1() {
        Input({ "#0",
                "msalmanhore"});
        Output({"10"});
    }

    void BeforeTestCase() {
        S = "";
    }

    void TestGroup1() {
        CASE(create_label(1), S = "sisalmanmelamarsalma");
        CASE(create_label(2), S = "kenapasalmansoalnyasusah");
        CASE(create_label(3), S = "salmaniniyanghardbisadpnggak");
        CASE(create_label(4), S = "almasalmanapakabardeadlineman");
        CASE(create_label(5), S = "salmasalmanslmansalmaansalan");
    }

private:
    void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}

};
