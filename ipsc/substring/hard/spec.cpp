#include <tcframe/spec.hpp>
using namespace tcframe;

#include <bits/stdc++.h>
using namespace std;

const int N_MAX = 1e6;

class ProblemSpec : public BaseProblemSpec {
protected:
    string LABEL;
    int N; // For readability at TestCases()
    string S;

    long long result;

    void InputFormat() {
        LINE(LABEL);
        LINE(S);
    }

    void OutputFormat() {
        LINE(result);
    }

    void Constraints() {
        CONS(inBetween(S.length(), 1, N_MAX));
    }

private:
    bool inBetween(int x, int lo, int hi) {
        return lo <= x && x <= hi;
    }
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:
    void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}

    void SampleTestCase1() {
        Input({ "#0",
                "msalmanhore"});
        Output({"10"});
    }

    void BeforeTestCase() {
        S = "";
    }

    void TestGroup1() {
        CASE(create_label(1), N = 757575, generateSalmanString(S, N));
        CASE(create_label(2), N = 999879, generateSalmanString(S, N));
        CASE(create_label(3), N = 596684, generateSalmanString(S, N, 0));
        CASE(create_label(4), N = 750339, generateSalmanString(S, N, 1));
        CASE(create_label(5), N = 999911, generateSalmanString(S, N, N/6));
    }

private:
    void addRandomCharacters(string &A, int size) {
        for (int i = 0; i < size; i++) {
            A += 'a' + rnd.nextInt(26);
        }
    }

    void addFixedCharacters(string &A, string B) {
        int size = B.length();
        for (int i = 0; i < size; i++) {
            A += B[i];
        }
    }

    void generateSalmanString(string &A, int size, int cnt = -1) {
        if (cnt == -1) {
            cnt = rnd.nextInt(0, size/6);
        } else {
            cnt = min(size/6, cnt);
        }
        int comp_size = size - 5*cnt;

        vector<int> _idx(comp_size);
        iota(_idx.begin(), _idx.end(), 0);
        rnd.shuffle(_idx.begin(), _idx.end());

        vector<int> idx;
        for (int i = 0; i < cnt; i++) idx.push_back(_idx[i]);
        sort(idx.begin(), idx.end());

        int previ = 0;
        for (int i : idx) {
            addRandomCharacters(A, i-previ);
            addFixedCharacters(A, "salman");
            previ = i+1;
        }
        addRandomCharacters(A, comp_size-previ);
    }
};
