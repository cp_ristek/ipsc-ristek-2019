#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0); cin.tie(0);
    
    string label;
    cin >> label;
    string K;
    cin >> K;
    
    int N = K.length();
    int last = -1;
    long long ans = 0;
    for (int i = 0; i+5 < N; i++) {
        if (K[i] == 's' && K[i+1] == 'a' && K[i+2] == 'l' && 
                K[i+3] == 'm' && K[i+4] == 'a' && K[i+5] == 'n') {
            ans += (long long)(i - last)*((N - 1) - (i + 4));
            last = i;
        }
    }
    cout << ans << "\n";
    
    return 0;
}
