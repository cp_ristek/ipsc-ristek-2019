import json, math

def is_legitimate_submission(submission):
    return 0 <= submission['time_from_start'] <= 120

problem_code = {
    'Desimal (Mudah)': 'A',
    'Desimal (Sulit)': 'B',
    'Donat Enak (Mudah)': 'C',
    'Donat Enak (Sulit)': 'D',
    'Substring Salman (Mudah)': 'E',
    'Substring Salman (Sulit)': 'F',
    'Membangun Tree (Mudah)': 'G',
    'Membangun Tree (Sulit)': 'H',
    'Kursi Kereta (Mudah)': 'I',
    'Kursi Kereta (Sulit)': 'J',
    'Pengurangan Matriks (Mudah)': 'K',
    'Pengurangan Matriks (Sulit)': 'L',
    'Formasi Raja (Mudah)': 'M',
    'Formasi Raja (Sulit)': 'N',
}

user_problem_history = {}
solved_penalty_of_user = {}

def add_one_solved_for_user(username, penalty):
    if username not in solved_penalty_of_user:
        solved_penalty_of_user[username] = (0, 0)

    cur_solved_penalty = solved_penalty_of_user[username]
    solved_penalty_of_user[username] = (cur_solved_penalty[0] + 1, cur_solved_penalty[1] + penalty)

def update_submission(submission):
    username = submission['hacker_username']
    problem = problem_code[submission['challenge']['name']]
    verdict = submission['status']
    time = submission['time_from_start']
    if username not in user_problem_history:
        user_problem_history[username] = {}

    if problem not in user_problem_history[username]:
        user_problem_history[username][problem] = (-1, 0)

    history = user_problem_history[username][problem]
    if history[0] == -1: # if not already got accepted
        result = history
        if verdict == 'Accepted':
            result = (time + 8*history[1], history[1])
            add_one_solved_for_user(username, result[0])
        elif verdict != 'Compilation error':
            result = (history[0], history[1] + 1)
        user_problem_history[username][problem] = result

def get_rank():
    rank = []
    for username in solved_penalty_of_user:
        solved_penalty = solved_penalty_of_user[username]
        solved_penalty = (solved_penalty[0], math.ceil(solved_penalty[1]))
        rank.append((solved_penalty, username))
    rank.sort(key=lambda solved_penalty_user:
        (-solved_penalty_user[0][0], solved_penalty_user[0][1]))

    for (solved_penalty, username) in rank:
        print(username, solved_penalty)

with open('submissions.json') as submissions_file:
    submissions = json.load(submissions_file)
    reversed_submissions = submissions['models'][::-1]
    for submission in reversed_submissions:
        if is_legitimate_submission(submission):
            update_submission(submission)
    get_rank()
