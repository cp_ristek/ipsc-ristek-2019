#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	long long A;
	string ANS;
	string LABEL;

	void InputFormat() {
		LINE(LABEL);
		LINE(A);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
	}

	void Constraints() {
		CONS(0 <= A && A <= 1e18);
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"15"});
		Output({"0.3636364"});
	}

	void TestGroup1() {
		for (int i=1;i<=10;++i)  {
			CASE(create_label(i), A=rnd.nextLongLong(0,1000000000000000000));
		}
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}
};

