#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0); cin.tie(0);
    
    string label;
    cin >> label;
    long long A;
    cin >> A;
    cout << fixed << setprecision(7) << (A % 11) / 11.0 << "\n";
    
    return 0;
}
