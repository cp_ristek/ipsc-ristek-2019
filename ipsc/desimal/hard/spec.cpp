#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	string A;
	string ANS;
	string LABEL;

	void InputFormat() {
		LINE(LABEL);
		LINE(A);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
	}

	void Constraints() {
		CONS(1 <= A.length() && A.length() <= 100000);
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"15"});
		Output({"0.3636364"});
	}

	void TestGroup1() {
		for (int i=1;i<=10;++i)  {
			CASE(create_label(i), generate(rnd.nextInt(50000, 99999)));
		}
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}
	
	void generate(int n) {
		A = "";
		for (int i = 0; i < n; i++) {
			A += rnd.nextInt(0, 9) + '0';
		}
	}
};
