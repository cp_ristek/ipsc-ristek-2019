#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0); cin.tie(0);
    
    string label;
    cin >> label;
    string A;
    cin >> A;
    int diff = 0;
    for (int i = 1; i < (int)A.length(); i++) {
        if (i&1) diff -= A[A.length()-1-i] - '0';
        else diff += A[A.length()-1-i] - '0';
    }
    diff = ((diff%11) - 11)%11;
    int diff_last = ((A.back() - '0') - abs(diff) + 11)%11;
    cout << fixed << setprecision(7) << diff_last / 11.0 << "\n";
    
    return 0;
}
