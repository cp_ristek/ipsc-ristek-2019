#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	int N;
	vector<long long> A;
	long long ANS;
	string LABEL;

	void InputFormat() {
		LINE(LABEL);
		LINE(N);
		LINE(A % SIZE(N));
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
	}

	void Constraints() {
		CONS(1 <= A.size() && A.size() <= 1000000);
		eachElementBetween(A, -1000000000000000000, 1000000000000000000);
	}
	
private:
	bool eachElementBetween(vector<long long> &v, long long l, long long r) {
		for (int num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"6",
				"4 2 6 3 3 18"});
		Output({"18"});
	}

	void TestGroup1() {
		CASE(create_label(1), N = rnd.nextInt(20, 25), generateUnbalanced(N));
		CASE(create_label(2), N = rnd.nextInt(20, 25), generateUnbalanced(N));
		CASE(create_label(3), N = 1, generateBalanced(N));
		for (int i=4;i<=10;++i) {
			CASE(create_label(i), N = rnd.nextInt(50000, 100000), generateBalanced(N));
		}
	}
	
	void BeforeTestCase() {
		A.clear();
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}
	
	void traverseUnbalanced(const vector<vector<int>> &child, int now) {
		if (child[now].empty()) A[now] = rnd.nextInt(-10000, 10000);
		else {
			for (int c: child[now]) {
				traverseUnbalanced(child, c);
				if (child[c].empty()) A[now] += A[c];
				else A[now] += 2*A[c];
			}
		}
	}
	
	void generateUnbalanced(int n) {
		vector<vector<int>> child(n);
		int last = 0;
		for (int i = 1; i < n; i++) {
			while (last+1 < i && rnd.nextInt(1, 2) == 1) {
				last++;
			}
			child[last].push_back(i);
			if ((int)child[last].size() == 2) last++;
		}
		
		A.resize(n);
		traverseUnbalanced(child, 0);
		rnd.shuffle(A.begin(), A.end());
	}

	long long traverseBalanced(int l, int r) {
		if (l > r) return 0;
		else if (l == r) {
			long long val = rnd.nextInt(-100000, 100000);
			A.push_back(val);
			return val;
		} else {
			int m = (l+r)/2;
			long long x = traverseBalanced(l, m-1);
			long long y = traverseBalanced(m+1, r);
			A.push_back(x+y);
			return 2*(x+y);
		}
	}

	void generateBalanced(int n) {
		traverseBalanced(0, n-1);
		rnd.shuffle(A.begin(), A.end());
	}
};
