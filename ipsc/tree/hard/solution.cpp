#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0); cin.tie(0);
    
    string label;
    cin >> label;
    int N;
    cin >> N;
    long long sum = 0;
    for (int i = 0; i < N; i++) {
        long long x;
        cin >> x;
        sum += x;
    }
    long long ans = N == 1 ? sum : sum/2;
    cout << ans << "\n";
    
    return 0;
}
