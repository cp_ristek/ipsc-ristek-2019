Membangun Tree

[Deskripsi Soal]

Universitas Indonesia memiliki hutan kota yang cukup luas dan memiliki banyak pohon. Tentu saja, salah satu pohon tersebut adalah tree pada teori graf! Sayangnya, ada seseorang tak bertanggungjawab yang menebang sebuah pohon dan memotongnya menjadi beberapa bagian. Pohon tersebut unik karena pada tiap percabangan, jumlah cabangnya **tidak lebih dari dua**. Selain itu, nilai dari suatu bagian merupakan jumlah nilai dari **semua bagian di bawahnya**, jika ada. Misalnya, jika ada bagian-bagian dengan nilai 4, 2, 6, 3, 3, dan 18, kita dapat membuat pohon seperti berikut:
{{tree.png}}

Dapat dilihat bahwa 3 merupakan penjumlahan bagian di bawahnya yaitu 3, 6 merupakan hasil dari 4 + 2, dan 18 merupakan hasil dari 4 + 2 + 3 + 6 + 3.

Sekarang, Anda diberikan bagian-bagian dari sebuah pohon, dapatkah Anda menentukan akar/root-nya? Akar/root dari sebuah pohon adalah bagian paling atas dari sebuah pohon.

[Format Masukan]

Baris pertama berisi label kasus uji.
Label kasus uji adalah suatu string "#S" dimana S adalah nomor kasus uji tersebut. Kasus uji contoh adalah kasus uji nomor 0.

Baris kedua berisi sebuah bilangan N, banyaknya bagian dari pohon.
Baris ketiga berisi N bilangan A_1, A_2, ..., A_N yaitu nilai masing-masing bagian.

[Format Keluaran]

Keluarkan sebuah baris berisi nilai dari akar/root pohon tersebut.

[Contoh Masukan]

#0
6
4 2 6 3 3 18

[Contoh Keluaran]

18

[Batasan Easy]

- 1 ≤ N ≤ 10
- 0 ≤ A_i ≤ 1.000.000.000
- Dijamin A_i merupakan bagian-bagian tree sesuai deskripsi soal.

[Batasan Hard]

- 1 ≤ N ≤ 100.000
- -10^18 ≤ A_i ≤ 10^18
- Dijamin A_i merupakan bagian-bagian tree sesuai deskripsi soal.
