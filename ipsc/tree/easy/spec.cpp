#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	int N;
	vector<int> A;
	int ANS;
	string LABEL;

	void InputFormat() {
		LINE(LABEL);
		LINE(N);
		LINE(A % SIZE(N));
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
	}

	void Constraints() {
		CONS(1 <= A.size() && A.size() <= 10);
		eachElementBetween(A, -1000000000, 1000000000);
	}
	
private:
	bool eachElementBetween(vector<int> &v, int l, int r) {
		for (int num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"6",
				"4 2 6 3 3 18"});
		Output({"18"});
	}

	void TestGroup1() {
		for (int i=1;i<=8;++i) {
			CASE(create_label(i), N = rnd.nextInt(5, 10), generate(N));
		}
		CASE(create_label(9), N = 1, generate(N));
		CASE(create_label(10), N = rnd.nextInt(5, 10), generate(N));
	}
	
	void BeforeTestCase() {
		A.clear();
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}
	
	void traverse(const vector<vector<int>> &child, int now) {
		if (child[now].empty()) A[now] = rnd.nextInt(0, 1000000);
		else {
			for (int c: child[now]) {
				traverse(child, c);
				if (child[c].empty()) A[now] += A[c];
				else A[now] += 2*A[c];
			}
		}
	}
	
	void generate(int n) {
		vector<vector<int>> child(n);
		int last = 0;
		for (int i = 1; i < n; i++) {
			while (last+1 < i && rnd.nextInt(1, 2) == 1) {
				last++;
			}
			child[last].push_back(i);
			if ((int)child[last].size() == 2) last++;
		}
		
		A.resize(n);
		traverse(child, 0);
		rnd.shuffle(A.begin(), A.end());
	}
};
