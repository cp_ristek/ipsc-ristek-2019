#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0); cin.tie(0);
    
    string label;
    cin >> label;
    int N;
    cin >> N;
    int mx = 0;
    for (int i = 0; i < N; i++) {
        int x;
        cin >> x;
        mx = max(mx, x);
    }
    cout << mx << "\n";
    
    return 0;
}
