#include <bits/stdc++.h>
using namespace std;

typedef pair<int, int> pii;
#define fi first
#define se second
#define mp make_pair

vector<int> arr_x, arr_y;

int main() {
    string label;
    cin >> label;
    int n; scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        int x, y;
        scanf("%d%d", &x, &y);
        x--; y--;
        arr_x.push_back(x);
        arr_y.push_back(y);
    }

    sort(arr_x.begin(), arr_x.end());
    sort(arr_y.begin(), arr_y.end());

    long long ans = 0;
    for (int i = 0; i < n; i++) {
        ans += abs(arr_x[i] - i);
        ans += abs(arr_y[i] - i);
    }

    printf("%lld\n", ans);
}
