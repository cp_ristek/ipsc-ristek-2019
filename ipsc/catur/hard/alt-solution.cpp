#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0); cin.tie(0);
    
    string label;
    cin >> label;
    int N;
    cin >> N;
    vector<int> r, c;
    for (int i = 0; i < N; i++) {
        int cr, cc;
        cin >> cr >> cc;
        r.push_back(cr);
        c.push_back(cc);
    }
    sort(r.begin(), r.end());
    sort(c.begin(), c.end());
    
    long long ans = 0;
    for (int i = 0; i < N; i++) {
        ans += abs(r[i] - (i+1));
        ans += abs(c[i] - (i+1));
    }
    cout << ans << "\n";
    
    return 0;
}
