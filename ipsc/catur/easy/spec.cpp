#include <tcframe/spec.hpp>
using namespace tcframe;

#include <bits/stdc++.h>
using namespace std;

const int N_MAX = 1e5;

class ProblemSpec : public BaseProblemSpec {
protected:
    string LABEL;
    int N;
    vector<int> X, Y;

    long long result;

    void InputFormat() {
        LINE(LABEL);
        LINE(N);
        LINES(X, Y) % SIZE(N);
    }

    void OutputFormat() {
        LINE(result);
    }

    void Constraints() {
        CONS(1 <= N && N <= 4);
        CONS(eachElementBetween(X, 1, N));
        CONS(eachElementBetween(Y, 1, N));
        CONS(eachPairDifferent(X, Y));
    }

private:

    bool eachElementBetween(vector<int> &A, int lo, int hi) {
        for (int x : A) {
            if (x < lo || x > hi) {
                return false;
            }
        }
        return true;
    }

    bool eachPairDifferent(vector<int> &A, vector<int> &B) {
        set<pair<int, int>> vis;
        int size = A.size();
        for (int i = 0; i < size; i++) {
            pair<int, int> tmp = make_pair(A[i], B[i]);
            if (vis.count(tmp)) return false;
            vis.insert(tmp);
        }
        return true;
    }

};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:
    void SampleTestCase1() {
        Input({
            "#0",
            "4",
            "1 1",
            "1 2",
            "2 3",
            "4 3",
        });
        Output({"3"});
    }

    void BeforeTestCase() {
        X.clear();
        Y.clear();
    }

    void AfterTestCase() {
        vector<int> idx(N);
        iota(idx.begin(), idx.end(), 0);
        rnd.shuffle(idx.begin(), idx.end());
        for (int i = 0; i < N; i++) {
            swap(X[i], X[idx[i]]);
            swap(Y[i], Y[idx[i]]);
        }
    }

    void TestGroup1() {
        // trivial
        CASE(create_label(1), N = 1, randomPairArray(X, Y, N));

        CASE(create_label(2), N = 4, randomPairArray(X, Y, N));
        CASE(create_label(3), N = 4, randomPairArray(X, Y, N));
        CASE(create_label(4), N = 4, randomPairArray(X, Y, N));
        CASE(create_label(5), N = 4, rectangleArray(X, Y, N));

    }

private:
    void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}

    void randomPairArray(vector<int> &A, vector<int> &B, int size) {
        set<pair<int, int>> vis;
        for (int i = 0; i < size; i++) {
            pair<int, int> tmp = make_pair(rnd.nextInt(1, size), rnd.nextInt(1, size));
            while (vis.count(tmp)) {
                tmp = make_pair(rnd.nextInt(1, size), rnd.nextInt(1, size));
            }
            vis.insert(tmp);
            A.push_back(tmp.first);
            B.push_back(tmp.second);
        }
    }

    void rectangleArray(vector<int> &A, vector<int> &B, int size) {
        int length = floor(sqrt(size));
        int A_i = 1;
        int B_i = 1;
        for (int i = 0; i < size; i++) {
            if (A_i > length) {
                A_i = 1; B_i++;
            }
            A.push_back(A_i);
            B.push_back(B_i);
            A_i++;
        }

        int A_offset = rnd.nextInt(size-1-length);
        int B_offset = rnd.nextInt(size-1-B_i);

        for (int i = 0; i < size; i++) {
            A[i] += A_offset;
            B[i] += B_offset;
        }
    }
};
