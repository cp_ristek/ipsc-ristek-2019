#include <bits/stdc++.h>
using namespace std;

const int dx[] = {0, -1, 0, 1};
const int dy[] = {-1, 0, 1, 0};

typedef pair<int, int> pii;
#define fi first
#define se second
#define mp make_pair

int n;
vector<pii> arr;

queue<vector<pii> > q;
set<vector<pii> > visited;

bool check(vector<pii> &v) {
    //printf("checking:"); goprint(v);
    set<pii> vis;
    for (pii tmp : v) {
        if (tmp.fi < 0 || tmp.fi >= n || tmp.se < 0 || tmp.se >= n || vis.count(tmp)) {
            return false;
        }
        vis.insert(tmp);
    }
    return true;
}

bool is_solution(vector<pii> &v) {
    set<int> vis_x, vis_y;
    for (pii tmp : v) {
        int x = tmp.fi, y = tmp.se;
        if (vis_x.count(x) || vis_y.count(y)) {
            return false;
        }
        vis_x.insert(x); vis_y.insert(y);
    }
    return true;
}

int bfs() {
    visited.insert(arr);
    q.push(arr);
    int cnt_now = 1;
    int cnt_next = 0;
    int step = 0;
    while (!q.empty()) {
        if (cnt_now == 0) {
            cnt_now = cnt_next;
            cnt_next = 0;
            step++;
        }
        vector<pii> tmp = q.front();
        q.pop();
        cnt_now--;

        //printf("now:"); goprint(tmp);

        if (is_solution(tmp)) {
            return step;
        }

        for (int i = 0; i < (int)tmp.size(); i++) {
            for (int j = 0; j < 4; j++) {
                tmp[i].fi += dx[j];
                tmp[i].se += dy[j];
                if (check(tmp) && !visited.count(tmp)) {
                    visited.insert(tmp);
                    q.push(tmp);
                    cnt_next++;
                }
                tmp[i].fi -= dx[j];
                tmp[i].se -= dy[j];
            }
        }
    }

    return -1;
}

int main() {
    string label;
    cin >> label;
    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        int x, y;
        scanf("%d%d", &x, &y);
        x--; y--;
        arr.push_back(mp(x, y));
    }

    printf("%d\n", bfs());
}
