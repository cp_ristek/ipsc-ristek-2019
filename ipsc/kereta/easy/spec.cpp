#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	int N;
	int ANS;
	string LABEL;

	void InputFormat() {
		LINE(LABEL);
		LINE(N);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
	}

	void Constraints() {
		CONS(1 <= N && N <= 20);
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"2"});
		Output({"5"});
	}

	void TestGroup1() {
        vector<int> query;
        for (int i = 0; i < 10; ++i) query.push_back(rnd.nextInt(2*i + 1, 2*i + 2));
        rnd.shuffle(query.begin(), query.end());
		for (int i = 0; i < 10; ++i) {
			CASE(create_label(i+1), N = query[i]);
		}
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}
};
