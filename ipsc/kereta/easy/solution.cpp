#include <bits/stdc++.h>
using namespace std;
const int MOD = 1000000007;
typedef long long ll;

ll pkt[25];

ll getBit(ll mask, ll pos) {
    return mask/pkt[pos]%3;
}

ll f(ll pos, ll mask) {
    if (pos == 0) return 1;
    ll result = 0;
    result += f(pos-1, mask + pkt[pos]) + f(pos-1, mask);
    if (getBit(mask, pos+1) == 1) result += f(pos-1, mask + 2*pkt[pos]);
    result %= MOD;
    return result;
} 

int main() {
    ios::sync_with_stdio(0); cin.tie(0);
    
    string label;
    cin >> label;
    long long N;
    cin >> N;
    pkt[0] = 1;
    for (ll i = 1; i < 25; i++) pkt[i] = 3*pkt[i-1];
    cout << f(N, 0) << "\n";
    
    return 0;
}
