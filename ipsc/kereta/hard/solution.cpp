#include <bits/stdc++.h>
using namespace std;
const int MOD = 1000000007;
typedef long long ll;
typedef vector<vector<ll>> mat;

mat kali(mat a, mat b) {
    mat res = mat(a.size(), vector<ll>(b[0].size()));
    for (int i = 0; i < (int)a.size(); i++) {
        for (int j = 0; j < (int)b[0].size(); j++) {
            for (int k = 0; k < (int)b.size(); k++) {
                res[i][j] = (res[i][j] + a[i][k] * b[k][j]) % MOD;
            }
        }
    }
    return res;
}

mat pow(mat a, ll p) {
    if (p == 1) return a;
    else if (p&1) return kali(pow(a, p-1), a);
    else {
        mat temp = pow(a, p/2);
        return kali(temp, temp);
    }
}

int main() {
    ios::sync_with_stdio(0); cin.tie(0);
    
    string label;
    cin >> label;
    ll N;
    cin >> N;
    mat trans = {{2, 1}, {1, 0}};
    mat init = {{1}, {0}};
    mat res = kali(pow(trans, N), init);
    cout << res[0][0] << "\n";
    
    return 0;
}
