#include <bits/stdc++.h>
using namespace std;
const int MOD = 1000000007;

int main() {
    ios::sync_with_stdio(0); cin.tie(0);
    
    string label;
    cin >> label;
    long long N;
    cin >> N;
    long long a = 1;
    long long b = 2;
    long long c = -1;
    for (long long i = 2; i <= N; i++) {
        c = (a + 2*b)%MOD;
        a = b;
        b = c;
        c = -1;
    }
    cout << b << "\n";
    
    return 0;
}
