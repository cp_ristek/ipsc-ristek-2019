#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	long long N;
	long long ANS;
	string LABEL;

	void InputFormat() {
		LINE(LABEL);
		LINE(N);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(120);
		MemoryLimit(256);
	}

	void Constraints() {
		CONS(1 <= N && N <= 10000000000);
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"2"});
		Output({"5"});
	}

	void TestGroup1() {
		for (int i = 0; i < 5; ++i) {
			CASE(create_label(i+1), N = rnd.nextLongLong(100, 1000000000));
		}
		for (int i = 0; i < 4; ++i) {
			CASE(create_label(i+6), N = rnd.nextLongLong(1000000000, 10000000000));
		}
		CASE(create_label(10), N = 10000000000);
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}
};
