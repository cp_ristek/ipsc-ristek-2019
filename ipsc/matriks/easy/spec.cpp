#include <tcframe/spec.hpp>
#define pb push_back

using namespace tcframe;
typedef long long LL;

class ProblemSpec : public BaseProblemSpec {
protected:
	string LABEL;
	LL N, M, ANS;
	vector<vector<LL> > A;
	vector<LL> B;

	void InputFormat() {
		LINE(LABEL);
		LINE(N, M);
		GRID(A) % SIZE(N, M);
		LINE(B % SIZE(2));
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= N && N <= 1000);
		CONS(2 <= M && M <= 3);
		CONS(eachElementV2DBetween(A, 0, (LL)1e8));
		CONS(eachElementV1DBetween(B, 1, (LL)1e8));
	}

private:
	bool eachElementV1DBetween(vector<LL> &v, LL l, LL r) {
		for (LL num : v) {
			if (num < l || num > r)
				return false;
		}
		return true;
	}
	bool eachElementV2DBetween(vector<vector<LL> > &v, LL l, LL r) {
		for (vector<LL> u : v) {
			for (LL num : u) {
				if (num < l || num > r)
					return false;
			}
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:
	const LL MAXX = (LL)1e8;
	void SampleTestCase1() {
		Input({
			"#0",
			"2 3",
			"1 3 0",
			"1 1 2",
			"1 2"
			});
		Output({"4"});
	}

	void TestGroup1() {
		CASE(create_label(1), N = rnd.nextInt(1, 1000), M = rnd.nextInt(2, 3), A = randomGrid(N, M, 0, 100), B = randomVector(2, 1, 100));
		CASE(create_label(2), N = rnd.nextInt(1, 1000), M = rnd.nextInt(2, 3), A = randomGrid(N, M, 0, 1000), B = randomVector(2, 1, 1000));
		CASE(create_label(3), N = rnd.nextInt(1, 1000), M = rnd.nextInt(2, 3), A = randomGrid(N, M, 0, 10000, 8), B = randomVector(2, 1, 10000));
		CASE(create_label(4), N = rnd.nextInt(1, 1000), M = rnd.nextInt(2, 3), A = randomGrid(N, M, 0, 1000000, 150), B = randomVector(2, 1, 1000000));
		CASE(create_label(5), N = 1000, M = rnd.nextInt(2, 3), A = randomGrid(N, M, 0, 100000000), B = randomVector(2, 1, 2));
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}

	vector<vector<LL> > randomGrid(LL N, LL M, LL lo, LL hi, LL num_0 = 0) {
		vector<vector<LL> > ans(N, vector<LL>(M, 0));
		for (LL i = 0; i < N; i++) {
			for (LL j = 0; j < M; j++) {
				ans[i][j] = rnd.nextInt(lo, hi);
			}
		}

		for (LL i = 0; i < num_0; i++) {
			LL r = rnd.nextInt(0, N - 1);
			LL c = rnd.nextInt(0, M - 1);
			ans[r][c] = 0;
		}
		return ans;
	}

	vector<LL> randomVector(LL N, LL lo, LL hi) {
		vector<LL> ans;
		for (LL i = 0; i < N; i++) {
			ans.pb(rnd.nextInt(lo, hi));
		}

		return ans;
	}
};
