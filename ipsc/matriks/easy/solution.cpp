#include <bits/stdc++.h>

using namespace std;
typedef long long LL;

LL N, M, A[1000][3];
LL B[2];
int main() {
	ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);

	string label;
	cin >> label;
	cin >> N >> M;
	for (LL i = 0; i < N; i++) {
		for (LL j = 0; j < M; j++) {
			cin >> A[i][j];
		}
	}

	cin >> B[0] >> B[1];
	
	LL ans = 0;

	if (B[1] > B[0]) {
    	for (LL i = 0; i < N; i++) {
    		for (LL j = M - 1; j >= 1; j--) {
    			LL times = (A[i][j] + B[1] - 1) / B[1]; // ceil(A[i][j] / B[1])
    			ans += times;
    			A[i][j] = max(0LL, A[i][j] - times * B[1]);
    			A[i][j - 1] = max(0LL, A[i][j - 1] - times * B[0]);
    		}
    		LL times = (A[i][0] + B[0] - 1) / B[0];
    		ans += times;
    		A[i][0] = max(0LL, A[i][0] - times * B[0]);
    	}
	} else {
    	for (LL i = 0; i < N; i++) {
    		for (LL j = 0; j < M - 1; j++) {
    			LL times = (A[i][j] + B[0] - 1) / B[0]; // ceil(A[i][j] / B[0])
    			ans += times;
    			A[i][j] = max(0LL, A[i][j] - times * B[0]);
    			A[i][j + 1] = max(0LL, A[i][j + 1] - times * B[1]);
    		}
    		LL times = (A[i][M - 1] + B[1] - 1) / B[1];
    		ans += times;
    		A[i][M - 1] = max(0LL, A[i][M - 1] - times * B[1]);
    	}    
	}

	cout << ans << endl;
	return 0;
}
