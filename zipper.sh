# run it in tc/
# bash ../../../../zipper.sh
# or bash zipper.sh if you copy it inside tc/

type=$(pwd | awk -F '/' '{ print $(NF-1) }')
slug=$(pwd | awk -F '/' '{ print $(NF-2) }')
numOfSample=$(ls | grep "sample.*\.in" | wc -l)
numOfTC=$(ls | grep -v "sample" | grep ".*\.in" | wc -l)

mkdir -p input
mkdir -p output
mkdir -p tc_input

for ((i=0; i < $numOfSample; i++))
do
cp "$type"_sample_$((i+1)).in input/input$i.txt
cp "$type"_sample_$((i+1)).out output/output$i.txt
cp "$type"_sample_$((i+1)).in tc_input/"$slug"-"$type"_$i.in

label=$(head -n 1 input/input$i.txt)
if [ $label != \#$i ]
then
    echo "$type"_sample_$((i+1)).in label is invalid
    exit
fi
done

for ((i=0; i < $numOfTC; i++))
do
cp "$type"_1_$((i+1)).in input/input$((i+numOfSample)).txt
cp "$type"_1_$((i+1)).out output/output$((i+numOfSample)).txt
cp "$type"_1_$((i+1)).in tc_input/"$slug"-"$type"_$((i+numOfSample)).in

label=$(head -n 1 input/input$((i+numOfSample)).txt)
if [ $label != \#$((i+numOfSample)) ]
then
    echo "$type"_1_$((i+1)).in label is invalid
    exit
fi
done

zip -FS -r tc_hackerrank.zip input output
zip -FS -j "$slug"-"$type"-tc.zip tc_input/*

rm -r input output tc_input


