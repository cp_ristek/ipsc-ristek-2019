#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	long long A,B;
	long long ANS;
	string LABEL;

	void InputFormat() {
		LINE(LABEL);
		LINE(A, B);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
	}

	void Constraints() {
		CONS(-1000000000000LL <= A && A <= 1000000000000LL);
		CONS(-1000000000000LL <= B && B <= 1000000000000LL);
	}

private:
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"5 8"});
		Output({"13"});
	}

	void TestGroup1() {
		for (int i=1;i<=5;++i) {
			CASE(create_label(i), A = rnd.nextLongLong(-1000000000000LL, 1000000000000LL) , B = rnd.nextLongLong(-1000000000000LL, 1000000000000LL));
		}
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}
};