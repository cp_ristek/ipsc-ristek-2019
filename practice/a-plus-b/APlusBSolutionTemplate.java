import java.util.Scanner;

public class APlusBSolutionTemplate {
    // Masukkan jawaban anda untuk setiap kasus uji didalam variabel dibawah ini,
    // Misalkan jawaban anda untuk kasus uji nomor 2 adalah 3, maka baris dibawah akan menjadi 'static long jawaban_2 = 3;' (tanpa tanda petik)
    // Apabila jawaban Anda melebihi batasan tipe data int pada Java, sisipkan karakter L setelah angka jawaban anda.
    // Contoh: 'static long jawaban_3 = 3000000000L; ' (tanpa tanda petik)
    static long jawaban_0 = 13;
    static long jawaban_1 = ;
    static long jawaban_2 = ;
    static long jawaban_3 = ;
    static long jawaban_4 = ;
    static long jawaban_5 = ;

    public static void main(String[] args) {
        String label = (new Scanner(System.in)).next();
        if ("#0".equals(label)) System.out.println(jawaban_0);
        if ("#1".equals(label)) System.out.println(jawaban_1);
        if ("#2".equals(label)) System.out.println(jawaban_2);
        if ("#3".equals(label)) System.out.println(jawaban_3);
        if ("#4".equals(label)) System.out.println(jawaban_4);
        if ("#5".equals(label)) System.out.println(jawaban_5);
    }
}