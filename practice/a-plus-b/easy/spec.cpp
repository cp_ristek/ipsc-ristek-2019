#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	int A,B;
	int ANS;
	string LABEL;

	void InputFormat() {
		LINE(LABEL);
		LINE(A, B);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
	}

	void Constraints() {
		CONS(-100000 <= A && A <= 100000);
		CONS(-100000 <= B && B <= 100000);
	}

private:
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"5 8"});
		Output({"13"});
	}

	void TestGroup1() {
		for (int i=1;i<=5;++i) {
			CASE(create_label(i), A = rnd.nextInt(-100000, 100000) , B = rnd.nextInt(-100000, 100000));
		}
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}
};