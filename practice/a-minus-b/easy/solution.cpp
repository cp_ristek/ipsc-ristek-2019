#include <bits/stdc++.h>
using namespace std;


struct{
	vector<int> solutions;

	void answer(int x){
		solutions.push_back(x);
	}

	void print_solution() {
		long long M = 1000000007;
		long long res = 0;
		long long p = 13;
		for (int sol : solutions){
			res = (res + (p*sol)%M)%M;
			p = (p*13)%M;
		}
		while (res<0) res+=M;
		cout << res << '\n';
	}

} solutionEngine;

string label;
int q;
int a,b;
int main() {
	cin >> label;
	cin >> q;
	for (int i=0;i<q;++i) {
		cin >> a >> b;
		solutionEngine.answer(a-b);
	}

	solutionEngine.print_solution();
	return 0;
}