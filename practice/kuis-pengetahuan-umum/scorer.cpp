#include <bits/stdc++.h>
using namespace std;

int wa(string msg) {
    cout << "WA" << endl;
    if (msg != "") cout << "[" << msg << "]" << endl;
    return 0;
}

int ok(string msg) {
	cout << "OK" << endl;
	cout << "0 ";
    if (msg != "") cout << "[" << msg << "]";
    cout << endl;
    return 0;
}

int ac() {
    cout << "AC" << endl;
    return 0;
}

/*
Questions:
Umur Badu sama dengan umur Budi dijumlahkan dengan umur Beni. Lima tahun yang lalu, umur Budi sama dengan sepuluh kali lipat umur Beni. Jika umur Budi saat ini 15 tahun, berapakah umur Badu saat ini?

Pada tahun berapa perang dunia kedua berakhir?

Berapa harga 1 botol air minum Prima (dalam Rupiah) di Kantin FASILKOM pada tahun 2017?

Pada tahun berapa Freddie Mercury wafat?

Apakah jawaban terbaik dari kehidupan, alam, dan segalanya?

*/

string Q1 = "Umur Badu sama dengan umur Budi dijumlahkan dengan umur Beni. Lima tahun yang lalu, umur Budi sama dengan sepuluh kali lipat umur Beni. Jika umur Budi saat ini 15 tahun, berapakah umur Badu saat ini?";
string Q2 = "Pada tahun berapa perang dunia kedua berakhir?";
string Q3 = "Berapa harga 1 botol air minum Prima (dalam Rupiah) di Kantin FASILKOM UI pada tahun 2017?";
string Q4 = "Pada tahun berapa Freddie Mercury wafat?";
string Q5 = "Apakah jawaban terbaik dari kehidupan, alam, dan segalanya?";


int main(int argc, char* argv[]) {
    ifstream tc_in(argv[1]);
    ifstream tc_out(argv[2]);
    ifstream con_out(argv[3]);

    int con_ans;

    if (!(con_out >> con_ans)) {
        return wa("");
    }

    if (con_ans == 21)
    	return ok(Q2);
    else if (con_ans == 1945)
    	return ok(Q3);
    else if (con_ans == 2500)
    	return ok(Q4);
    else if (con_ans == 1991)
    	return ok(Q5);
    else if (con_ans == 42)
    	return ac();
    
    return wa("");
}