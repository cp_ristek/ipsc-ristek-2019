#include <bits/stdc++.h>

using namespace std;

/*
Questions:
Umur Badu sama dengan umur Budi dijumlahkan dengan umur Beni. Lima tahun yang lalu, umur Budi sama dengan sepuluh kali lipat umur Beni. Jika umur Budi saat ini 15 tahun, berapakah umur Badu saat ini?
Ans: 21
Pada tahun berapa perang dunia kedua berakhir?
Ans: 1945
Berapa harga 1 botol air minum Prima (dalam Rupiah) di Kantin FASILKOM UI pada tahun 2017?
Ans: 2500
Pada tahun berapa Freddie Mercury wafat?
Ans: 1991
Apakah jawaban terbaik dari kehidupan, alam, dan segalanya?
Ans: 42
*/

int main() {
	cout << 42 << '\n';
}