#include <bits/stdc++.h>

using namespace std;

long long N;
int ans;
string label;
int main() {
	cin >> label;
	cin >> N;
	for (long long i=1;i*i<=N;++i) {
		if (N%i == 0){
			++ans;
			if (N/i != i) ++ans;
		}
	}
	cout << ans << '\n';
}