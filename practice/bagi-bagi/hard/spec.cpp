#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	long long N;
	long long ANS;
	string LABEL;
	
	void InputFormat() {
		LINE(LABEL);
		LINE(N);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= N && N <= 1000000000000LL);
	}

private:
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"40",});
		Output({"8"});
	}

	void TestGroup1() {
		CASE(create_label(1), N = rnd.nextLongLong(500000000000LL, 1000000000000LL));
		CASE(create_label(2), N = rnd.nextLongLong(500000000000LL, 1000000000000LL));
		CASE(create_label(3), N = rnd.nextLongLong(500000000000LL, 1000000000000LL));
		CASE(create_label(4), N = rnd.nextLongLong(500000000000LL, 1000000000000LL));
		CASE(create_label(5), N = 100000000183);
		CASE(create_label(6), N = sqr(rnd.nextLongLong(500,1000)));
		CASE(create_label(7), N = rnd.nextLongLong(500000000000LL, 1000000000000LL));
		CASE(create_label(8), N = 1000000000000LL);
		CASE(create_label(9), N = sqr(rnd.nextLongLong(500000, 1000000)));
		CASE(create_label(10), N = rnd.nextLongLong(500000000000LL, 1000000000000LL));
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}

	long long sqr(long long x) {
		return x*x;
	}
};