#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	int N;
	int ANS;
	string LABEL;
	
	void InputFormat() {
		LINE(LABEL);
		LINE(N);
	}

	void OutputFormat() {
		LINE(ANS);
	}

	void StyleConfig() {
		TimeLimit(1);
	}

	void Constraints() {
		CONS(1 <= N && N <= 1000000);
	}

private:
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	void SampleTestCase1() {
		Input({"#0",
				"40",});
		Output({"8"});
	}

	void TestGroup1() {
		CASE(create_label(1), N = 1);
		CASE(create_label(2), N = sqr(rnd.nextInt(2,1000)));
		CASE(create_label(3), N = rnd.nextInt(500000, 1000000));
		CASE(create_label(4), N = rnd.nextInt(3,1000000));
		CASE(create_label(5), N = 7);
		CASE(create_label(6), N = sqr(rnd.nextInt(500,1000)));
		CASE(create_label(7), N = rnd.nextInt(500000, 1000000));
		CASE(create_label(8), N = rnd.nextInt(500000, 1000000));
		CASE(create_label(9), N = sqr(rnd.nextInt(700,1000)));
		CASE(create_label(10), N = 1000000);
	}

private:
	void create_label(int i) {
		ostringstream ss;
		ss << i;
		LABEL = "#" + ss.str();
	}

	int sqr(int x) {
		return x*x;
	}
};