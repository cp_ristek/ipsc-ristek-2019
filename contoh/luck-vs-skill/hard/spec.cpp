#include <tcframe/spec.hpp>
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
	int N;
	
	void InputFormat() {
	}

	void OutputFormat() {
		LINE(N);
	}

	void StyleConfig() {
		TimeLimit(1);
		MemoryLimit(256);
		CustomScorer();
	}

	void Constraints() {
		CONS(1 <= N && N <= 2000);
	}

private:
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:

	
	void TestGroup1() {
		CASE(N = 912);
	}

private:
};