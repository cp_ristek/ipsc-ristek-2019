#include <bits/stdc++.h>
using namespace std;

int wa(string msg) {
    cout << "WA" << endl;
    if (msg != "") cout << "[" << msg << "]" << endl;
    return 0;
}

int ac() {
    cout << "AC" << endl;
    return 0;
}

int main(int argc, char* argv[]) {
    ifstream tc_in(argv[1]);
    ifstream tc_out(argv[2]);
    ifstream con_out(argv[3]);

    int con_ans;
    int tc_ans;
    int min_range=8, max_range=8;
    
    tc_out >> tc_ans;

    if (!(con_out >> con_ans)) {
        return wa("");
    }

    if (con_ans < min_range || con_ans > max_range)
    	return wa("");

    if (con_ans < tc_ans)
    	return wa("<");
    else if (con_ans > tc_ans)
    	return wa(">");
    else
    	return ac();
}