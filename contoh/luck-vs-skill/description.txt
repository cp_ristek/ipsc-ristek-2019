Luck vs Skill

[Deskripsi Soal]

Mari bermain!

Juri mempunyai sebuah angka favorit N. Tugas Anda adalah menebak angka favorit tersebut.

Anda mempunyai kesempatan tak terbatas dalam menebak.

Pada saat Anda menebak, Juri bisa memberi respon menggunakan verdict/keputusan yang diberikan:

- Wrong Answer
	- Terdapat 3 kemungkinan:
	- Tebakan Anda lebih besar dari N. Hal ini dapat dilihat dari pesan berupa simbol '>' disamping nilai Anda pada detil pengumpulan program Anda.
	- Tebakan Anda lebih kecil dari N. Hal ini dapat dilihat dari pesan berupa simbol '<' disamping nilai Anda pada detil pengumpulan program Anda.
	- Tebakan Anda diluar kemungkinan nilai N (dapat dilihat di batasan). Hal ini dapat dilihat dari tidak adanya pesan yang diberikan disamping nilai Anda pada detil pengumpulan program Anda.

- Accepted
	- Tebakan Anda adalah angka favorit juri.

Jangan lupa bahwa ada penalti untuk setiap program yang gagal mendapatkan Accepted, namun baru dihitung setelah Anda mendapatkan Accepted pada program yang bersangkutan.

[Format Masukan]

Tidak ada masukan dalam soal ini.

[Format Keluaran]

Keluarkan sebuah bilangan bulat yaitu tebakan Anda.

[Contoh Masukan]

Tidak ada masukan.

[Contoh Keluaran]

3

[Penjelasan Contoh]

Untuk contoh, misalkan batasan yang diberikan adalah 1 ≤ N ≤ 5 dengan angka favorit juri 3.

Apabila Anda menjawab 1 atau 2, Anda akan mendapatkan keputusan Wrong Answer dengan pesan berupa simbol '<'.
Apabila Anda menjawab 4 atau 5, Anda akan mendapatkan keputusan Wrong Answer dengan pesan berupa simbol '>'.
Apabila Anda menjawab diluar batasan, Anda akan mendapatkan keputusan Wrong Answer tanpa pesan apapun.

Diketahui pada contoh, Anda menjawab dengan angka 3. Maka Anda mendapat keputusan Accepted.

[Batasan Easy]

- 7 < N < 9

[Batasan Hard]

1 ≤ N ≤ 2.000